package controller;
import java.util.ArrayList;

import model.Aluno;
import model.Disciplina;

public class ControllerDisciplina{
	private ArrayList<Disciplina> listaDisciplinas;
	private ArrayList<Aluno> listaAlunos;
	
	public ControllerDisciplina(){
		this.listaDisciplinas = new ArrayList<Disciplina>();
		this.listaAlunos = new ArrayList<Aluno>();
	}

	public String adicionar(Disciplina umaDisciplina){
		this.listaDisciplinas.add(umaDisciplina);
		return "Disciplina adicionada com sucesso";
	}
	
	public String adicionarAluno(Aluno umAluno, Disciplina umaDisciplina){
		this.listaAlunos.add(umAluno);
		umaDisciplina.setListaAluno(this.listaAlunos);
		this.listaDisciplinas.remove(umaDisciplina);
		this.listaDisciplinas.add(umaDisciplina);
		return "Aluno adicionado a disciplina com sucesso";
	}

	public String excluir(Disciplina umaDisciplina){
		this.listaDisciplinas.remove(umaDisciplina);
		return "Disciplina excluida com sucesso";
	}

	public Disciplina localizar(String nome){
		for(Disciplina umaDisciplina : listaDisciplinas){
			if(umaDisciplina.getNome().equalsIgnoreCase(nome)){
				return umaDisciplina;			
			}
		}
		return null;
	}
	
	public ArrayList<Aluno> getListaAlunosDaDisciplina(Disciplina umaDisciplina){
		return umaDisciplina.getListaAluno();
	}
}
