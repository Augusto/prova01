public class Aluno{
	
	private String nome;
	private String matricula;
	
	public Aluno(){
	}
	public Aluno(String umNome , String umaMatricula){
		this.nome=umNome;
		this.matricula=umaMatricula;
	}
	
	public void setNome(String umNome){
		this.nome=umNome;
	}
	public String getNome(){
		return this.nome;
	}
	public void setMatricula(String umaMatricula){
		this.matricula=umaMatricula;
	}
	public String getMatricula(){
		return this.matricula;
	}
			
}
