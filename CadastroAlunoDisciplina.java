import java.io.*;
import java.util.*;

public class CadastroAlunoDisciplina{

	public static void main(String[] args) throws IOException{
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		
		ControllerAluno umControllerAluno = new ControllerAluno(); 
		ControllerDisciplina umControllerDisciplina = new ControllerDisciplina();
		
		int numeroMenu;
		String mensagem;
		
		do{
			System.out.println("=====Menu=====");
			System.out.println("1 - Adicionar Aluno ");
			System.out.println("2 - Excluir Aluno");
			System.out.println("3 - Localizar Aluno");
			System.out.println("4 - Adicionar Disciplina");
			System.out.println("5 - Excluir Disciplina");
			System.out.println("6 - Localizar Disciplina ");
			System.out.println("7 - Adicionar Aluno na Disciplina ");
			System.out.println("Insira opção:");
			numeroMenu = Integer.parseInt(leitorEntrada.readLine());
			
			Aluno umAluno = new Aluno(); 
			Disciplina umaDisciplina = new Disciplina();
			
	
			switch(numeroMenu){
				case 0:
					numeroMenu = 0;
					break;
				case 1:
					System.out.println("Insira o nome do aluno: ");
					umAluno.setNome(leitorEntrada.readLine());
					System.out.println("Insira a matricula: ");
					umAluno.setMatricula(leitorEntrada.readLine());
					mensagem = umControllerAluno.adicionar(umAluno);
					System.out.println(mensagem);
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 2:
					System.out.println("Digite o nome do aluno que deseja excluir: ");
					umAluno = umControllerAluno.localizar(leitorEntrada.readLine());
					if(umAluno != null){
						mensagem = umControllerAluno.excluir(umAluno);
					}
					else{
						mensagem = "Aluno não encontrado";
					}
					System.out.println(mensagem);
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 3:
					System.out.println("Digite o nome do aluno que deseja localizar: ");
					umAluno = umControllerAluno.localizar(leitorEntrada.readLine());
					if(umAluno != null){
						System.out.println("Nome: " + umAluno.getNome());
						System.out.println("Matricula: " + umAluno.getMatricula());
					}
					else{
						System.out.println("Aluno não encontrado");	
					}
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 4:
					System.out.println("Digite o nome da disciplina: ");
					umaDisciplina.setNome(leitorEntrada.readLine());
					System.out.println("Digite a turma: ");
					umaDisciplina.setTurma(leitorEntrada.readLine());
					mensagem = umControllerDisciplina.adicionar(umaDisciplina);
					System.out.println(mensagem);
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 5:
					System.out.println("Digite o nome da disciplina que deseja excluir: ");
					umaDisciplina = umControllerDisciplina.localizar(leitorEntrada.readLine());
					if(umaDisciplina != null){
						mensagem = umControllerDisciplina.excluir(umaDisciplina);
					}
					else{
						mensagem = "Disciplina não encontrada";
					}
					System.out.println(mensagem);
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 6:
					System.out.println("Digite o nome da disciplina que deseja localizar: ");
					umaDisciplina = umControllerDisciplina.localizar(leitorEntrada.readLine());
					if(umaDisciplina != null){
						System.out.println("Nome: " + umaDisciplina.getNome());
						System.out.println("Turma: " + umaDisciplina.getTurma());
						System.out.println("Lista de alunos matriculados na disciplina: ");
						ArrayList<Aluno> listaAlunos = umaDisciplina.getListaAluno();

						if(listaAlunos.size() != 0){
							for(int i = 0; i < listaAlunos.size(); i++){
	
								System.out.println("	" + i + ". " + listaAlunos.get(i).getNome());
	
							}
						}
						else{
							System.out.println("Nenhum aluno cadastrado na disciplina");
						}
					}
					else{
						System.out.println("Disciplina não encontrada");	
					}
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				case 7:
					System.out.println("Digite o nome da disciplina que deseja adicionar o aluno: ");
					umaDisciplina = umControllerDisciplina.localizar(leitorEntrada.readLine());
					if(umaDisciplina == null){
						System.out.println("Disciplina não encontrada");
						System.out.println("Pressione qualquer tecla para voltar ao menu");
						leitorEntrada.readLine();
						break;
					}
					System.out.println("Digite o nome do aluno que deseja adicionar a disciplina: ");
					umAluno = umControllerAluno.localizar(leitorEntrada.readLine());
					if(umAluno == null){
						System.out.println("Aluno não encontrado");
						System.out.println("Pressione qualquer tecla para voltar ao menu");
						leitorEntrada.readLine();
						break;
					}
					mensagem = umControllerDisciplina.adicionarAluno(umAluno, umaDisciplina);
					System.out.println(mensagem);
					System.out.println("Pressione qualquer tecla para voltar ao menu");
					leitorEntrada.readLine();
					break;
				default:
					System.out.println("Opção não válida, digite uma opção válida");
					break;
			}
		}

		while(numeroMenu != 0);
	}
}
