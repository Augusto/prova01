import java.util.ArrayList;


public class ControllerAluno{
	private ArrayList<Aluno> listaAlunos;
	
	public ControllerAluno(){
		this.listaAlunos = new ArrayList<Aluno>();
	}

	public String adicionar(Aluno umAluno){
		this.listaAlunos.add(umAluno);
		return "Aluno adicionado com sucesso";
	}
	
	public String excluir(Aluno umAluno){
		this.listaAlunos.remove(umAluno);
		return "Aluno excluido com sucesso";
	}

	public Aluno localizar(String nome){
		for(Aluno umAluno : listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(nome)){
				return umAluno;			
			}
		}
		return null;
	}

}
