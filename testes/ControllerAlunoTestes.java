package testes;
import controller.*;
import model.*;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ControllerAlunoTestes {
	static ControllerAluno novoControllerAluno;
	static Aluno novoAluno;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		novoControllerAluno = new ControllerAluno();
		novoAluno = new Aluno("Joao", "123");
	}
	
	@Test
	public void adicionarTest() {
		novoControllerAluno.adicionar(novoAluno);
		assertNotNull(novoControllerAluno.localizar("Joao"));
	}
	
	@Test
	public void excluirTest(){
		novoControllerAluno.excluir(novoAluno);
		assertNull(novoControllerAluno.localizar("Joao"));
	}

	
	@Test
	public void localizarTest(){
		novoControllerAluno.adicionar(novoAluno);
		assertNotNull(novoControllerAluno.localizar("Joao"));
	}
}
