package testes;
import controller.*;
import model.*;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ControllerDisciplinaTeste {
	static ControllerDisciplina novoControllerDisciplina;
	static ControllerAluno novoControllerAluno;
	static Disciplina novaDisciplina;
	static Aluno novoAluno;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		novoControllerDisciplina = new ControllerDisciplina();
		novoControllerAluno = new ControllerAluno();
		novaDisciplina = new Disciplina();
		novoAluno = new Aluno("joao", "123");
		novaDisciplina.setNome("OO");
	}

	@Test
	public void adicionarTest() {
		novoControllerDisciplina.adicionar(novaDisciplina);
		assertNotNull(novoControllerDisciplina.localizar("OO"));
	}
	
	@Test
	public void excluirTest() {
		novoControllerDisciplina.excluir(novaDisciplina);
		assertNull(novoControllerDisciplina.localizar("OO"));

	}
	
	@Test
	public void localizarTest() {
		novoControllerDisciplina.adicionar(novaDisciplina);
		assertNotNull(novoControllerDisciplina.localizar("OO"));
	}
	
	@Test
	public void AdicionarAlunoTest() {
		novoControllerDisciplina.adicionar(novaDisciplina);
		novoControllerAluno.adicionar(novoAluno);
		novoControllerDisciplina.adicionarAluno(novoAluno, novaDisciplina);
		assertNotNull(novoControllerDisciplina.getListaAlunosDaDisciplina(novaDisciplina));
	}
}
