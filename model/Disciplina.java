package model;

import java.util.ArrayList;

public class Disciplina{
	private String nome;
	private String turma;
	private ArrayList<Aluno> listaAluno;

	public Disciplina(){
		listaAluno = new ArrayList<Aluno>();
	}
	
	public void setNome(String umNome){
		this.nome = umNome;
	}
 	public String getNome(){
		return this.nome;
	}
	public void setTurma(String umaTurma){
		this.turma = umaTurma;
	}
 	public String getTurma(){
		return this.turma;
	}
	
	public void setListaAluno(ArrayList<Aluno> umaListaAluno){
		this.listaAluno = umaListaAluno;
	}
	
 	public ArrayList<Aluno> getListaAluno(){
		return this.listaAluno;
	}

}
